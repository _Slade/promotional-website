<?php
  require_once 'util.php';

  if (is_logged_in() && $_COOKIE['user_id'] == 'admin') {
    echo <<<EOT
    <aside id="right" class="column">
      <header id="newpost-header" class="mini-header">
        <h2>New post</h2>
      </header>
      <form name="postForm" id="postForm">
        <input type="text" id="postTitleInput" name="postTitle"
          placeholder="Title" required="true">
        </input>
        <textarea id="postBody" name="postBody" spellcheck="true"
          placeholder="New post" wrap="soft" required="true"
          value=""></textarea>
        <input type="button" name="postSubmit" id="postSubmit"
          value="Submit post">
        </input>
      </form>
    </aside>
EOT;
  }
?>
