<!DOCTYPE html>
<html>
  <head>
    <?php readfile('head.html') ?>
    <link rel="stylesheet" href="css/contact.css" />
  </head>
  <body>
    <?php readfile('header.html') ?>

    <main id="left" class="column">
      <div id="main-container">
        <header id="main-header" class="mini-header">
          <h2>Contact</h2>
        </header>

        <div id="contact">
          <dl>
            <dt>Email</dt>
            <dd>
              <a href="mailto:email@example.com">email@example.com</a>
            </dd>
            <dt>Public key</dt>
            <dd>
              <span class="key-fingerprint">
                53BC F6F6 14F3 5A27 29EE  90E9 FE7A 8CE9 405A 8692
              </span>
            </dd>
          </dl>
        </div>
      </div>
    </main>

    <?php readfile('nav.html') ?>

    <?php require_once('login_form.php') ?>

    <?php readfile('footer.html') ?>

    <?php readfile('endscripts.html') ?>
  </body>
</html>
