    <aside id="right" class="column">
      <header id="login-header" class="mini-header">
        <h2>Log In</h2>
      </header>
      <noscript>
        <p>Warning: This probably won't work with JavaScript disabled.</p>
      </noscript>
      <form name="login" id="loginForm">
      <?php
        require_once 'util.php';

        if (is_logged_in()) {
          printf(
            '<label class="centered">You are logged in as %s.</label>',
            $_COOKIE['user_id']
          );
          echo '<input type="button" id="logoutSubmit" name="logoutSubmit"',
           ' value="Log out" />';
        }
        else {
          echo '<label for="username" class="left">Username</label>';
          echo '<input type="text" name="username" required="true"',
           ' placeholder="Username" />';
          echo '<label for="password" class="left">Password</label>';
          echo '<input type="password" name="password" required="true"',
            ' placeholder="Password" />';
          echo '<input type="button" id="loginSubmit" name="loginSubmit"',
            ' value="Log in" />';
        }
      ?>
      </form>
    </aside>
