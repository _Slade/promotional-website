# Changes Since Part 1

* Converted all HTML pages to PHP (except for some static HTML includes).
* Deduplicated page headers, footers, nav boxes, etc. using PHP's file
inclusion mechanisms.
* Implemented retrieval of posts from database so they can be rendered on the
index page.
* Implemented post archive, which shows a list of posts, links to them, and
their creation times.
* Added a page (post.php) to render each post as its own page.
* Implemented pagination on the index.
* Added log-in functionality for the admin using JavaScript, AJAX, and the
database.
* Added the ability to make new posts on the website using JavaScript, AJAX,
and the database.

Details regarding how all these things were implemented are in the next
section.

# Project Layout

The website has 4 main pages: an index, a contact page, an "about me" page, and
a post archive. This README describes each page and which files are relevant to
it.

## All Pages

### Navigation

All pages have a navigation box in the right column. It has a list of links to
the other pages of the site, plus an unimplemented search bar.

Relevant files: [nav.html](nav.html), [search.js](scripts/search.js).

### Logging In

All pages have a log-in form in the right column. Unfortunately there's
no feature to create or change accounts — for the demonstration I
created one with the name "admin," the salt "salt," and the password hash
"7a37b85c8918eac19a9089c0fa5a2ab4dce3f90528dcdeec108b23ddf3607b99."

Logging in is done with AJAX and PHP. I used JavaScript to send an
XMLHttpRequest to the server, which sends a response in JSON indicating how the
browser should prompt the user. The server tracks logged-in users via cookies
and the `Sessions` table. When a user logs in, `user_id` and `session_id`
cookies are created, and an entry with that information is stored in the
database as well. When a user performs an action that requires authentication,
the values of the cookies are checked against the entries in the database.

I made some effort to make the whole process secure. On the secure side:

* Passwords are never stored except in salted and hashed form.
* The use of cookies with unique session IDs is in principle secure (I think).

On the insecure side:

* Session IDs *might* be predictable, since I just used PHP's `uniqid()`
function.
* Passwords are transmitted in cleartext because I haven't configured SSL.
* I don't log log-in attempts, even though I should.

Relevant files: [login_form.php](login_form.php), [login.php](login.php),
[login.js](scripts/login.js).

### Logging Out

Logging out is also handled with AJAX and PHP. When a user is logged in, a
"Log out" button will be shown in place of the usual log-in form. Clicking it
will send a request to the server that will cause it to delete the entry from
`Sessions` for that user and delete the user's log-in cookies.

Relevant files: [login_form.php](login_form.php), [login.php](login.php),
[login.js](scripts/login.js).

## Index

This is implemented in PHP. All the posts there are loaded from the database,
and excerpts from the most recent 10 (or whatever the `number` parameter is set
to in the query string) posts are shown. It generates links to go forward or
backward in time when appropriate.

Relevant files: [index.php](index.php), [posts.php](posts.php),
[util.php](util.php).

### Posting

When logged in, the admin is able to post via a form in the right column. This
is done with AJAX and PHP. Posts are stored in the database.

Relevant files: [new_post_form.php](new_post_form.php),
[new_post.php](new_post.php), [post.js](scripts/post.js).

### Posts

Posts are only shown in excerpted form on the index. Clicking a post's title
(or the "continue reading" link on an excerpted post) will bring the user to a
generated page with the full content of that post.

Posts can contain math, which is rendered with MathJAX.

Relevant files: [index.php](index.php) (page for displaying posts),
[post.php](post.php) (the page for displaying a single), [posts.php](posts.php)
(functions for handling posts).

## About Me

This page contains some text that's read in from `contents/about.html`. Other
than that it's mostly static HTML.

Relevant files: [about.php](about.php), [about.html](content/about.html).

## Contact

This page contains contact information. It's mostly static HTML.

Relevant file: [contact.php](contact.php).

## Miscellaneous Files

* [util.php](util.php): Miscellaneous utility functions in PHP (mostly for
dealing with the database).
* [head.html](head.html), [header.html](header.html),
[footer.html](footer.html), [endscripts.html](endscripts.html),
[nav.html](nav.html): Static HTML includes for the main pages.
* [init_db.sql](init_db.sql): SQL script for initializing the database tables.
* [db_dump.sql](content/db_dump.sql): A database dump of the test data I
was using, so you don't have to go to the trouble of manually initializing
everything.
