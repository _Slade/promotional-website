<!DOCTYPE html>
<html>
  <head>
    <?php readfile('head.html') ?>
    <link rel="stylesheet" href="css/archive.css" />
  </head>
  <body>
    <?php readfile('header.html') ?>
    <main id="left" class="column">
      <div id="main-container">
        <header id="main-header" class="mini-header">
          <h2>Post Archive</h2>
        </header>

        <!-- TODO: Get posts from database and return them -->
        <!-- Use query parameters to determine which posts to get -->
        <ul id="posts">
        <?php
          include 'posts.php';
          make_archive_list();
        ?>
        </ul>

      </div>
    </main>

    <?php readfile('nav.html') ?>

    <?php require_once('login_form.php') ?>

    <?php readfile('footer.html') ?>

    <?php readfile('endscripts.html') ?>
  </body>
</html>
