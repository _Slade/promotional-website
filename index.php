<!DOCTYPE html>
<html>
  <head>
    <?php readfile('head.html') ?>
    <link rel="stylesheet" href="css/index.css" />
    <link rel="stylesheet" href="css/post.css" />
  </head>
  <body>
    <?php readfile('header.html') ?>
    <main id="left" class="column">
      <div id="main-container">
<!-- Show this after we know no errors occurred
        <header id="main-header" class="mini-header">
          <h2>Recent Posts</h2>
        </header>
-->
        <?php
          include 'posts.php';

          function get_post_params() {
            parse_str($_SERVER['QUERY_STRING'], $query);
            $offset = array_key_exists('offset', $query)
              && is_numeric($query['offset'])
              ? $query['offset']
              : 0;
            $number = array_key_exists('number', $query)
              && is_numeric($query['number'])
              ? $query['number']
              : 10;
            return array($offset, $number);
          }

          call_user_func(
            function() {
              list($offset, $number) = get_post_params();
              $posts = get_post_range($offset, $number);
              if (is_null($posts)) {
                render_error('Error retrieving posts from database.');
                return;
              }
              foreach ($posts as &$post) {
                echo render_post($post, true), "\n";
              }
            }
          );
        ?>

      </div>
      <div id="pagination-buttons">
        <?php
          call_user_func(
            function() {
              list($offset, $number) = get_post_params();
              $post_count = get_post_count();
              if ($offset + $number < $post_count) {
                printf(
                  '<a id="prev" href="index.php?number=%s&offset=%s">'
                  . '&#x2190; Older posts</a>',
                  $number,
                  $offset + $number
                );
              }
              if ($offset > 0) {
                printf(
                  '<a id="next" href="index.php?number=%s&offset=%s">'
                  . 'Newer posts &#x2192;</a>',
                  $number,
                  $offset - $number
                );
              }
            }
          );
        ?>
      </div>
    </main>

    <?php readfile('nav.html') ?>

    <?php require_once('login_form.php') ?>

    <?php require_once('new_post_form.php') ?>

    <?php readfile('footer.html') ?>

    <?php readfile('endscripts.html') ?>
    <!-- Posting is only on the index page -->
    <script src="scripts/post.js"></script>
  </body>
</html>
