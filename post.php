<!DOCTYPE html>
<html>
  <head>
    <?php readfile('head.html') ?>
    <link rel="stylesheet" href="css/post.css" />
    <script src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-MML-AM_CHTML'></script>
  </head>
  <body>
    <?php readfile('header.html') ?>
    <main id="left" class="column">
      <div id="main-container">
        <?php
          require_once 'posts.php';
          call_user_func(
            function() {
              parse_str($_SERVER['QUERY_STRING'], $query);
              if (!array_key_exists('id', $query)) {
                render_error('Invalid or missing post ID.');
                return;
              }
              $post = get_post($query['id']);
              if (is_null($post)) {
                render_error('Error retrieving post from database.');
                return;
              }
              echo render_post($post, false), "\n";
            }
          );
        ?>
      </div>
    </main>

    <?php readfile('nav.html') ?>

    <?php readfile('footer.html') ?>

    <?php readfile('endscripts.html') ?>
  </body>
</html>
