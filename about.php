<!DOCTYPE html>
<html>
  <head>
    <?php readfile('head.html') ?>
    <link rel="stylesheet" href="css/about.css" />
  </head>
  <body>
    <?php readfile('header.html') ?>

    <main id="left" class="column">

      <div class="main-container">

        <header id="main-header" class="mini-header">
          <h2>About</h2>
        </header>

        <article id="aboutMe">
          <img id="aboutPortrait" src="img/placeholder.png" alt="Portrait" />

          <?php
            if (file_exists('content/about.html')) {
              readfile('content/about.html');
            }
            else {
              echo '<p> Uh oh, the about article is missing. </p>';
            }
          ?>

          <p>
            <a href="content/resume.pdf">My r&#x00E9;sum&#x00E9;.</a>
          </p>
        </article>


      </div>

    </main>

    <?php readfile('nav.html') ?>

    <?php require_once('login_form.php') ?>

    <?php readfile('footer.html') ?>

    <?php readfile('endscripts.html') ?>
  </body>
</html>
