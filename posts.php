<?php

require_once 'util.php';

define('POST_FMT', <<<EOT
        <article class="post">
          <header class="post-header">
            <h3> <a href="post.php?id=%s"> %s </a> </h3>
          </header>
          <section class="post-body">
            %s
          </section>
          <footer>
            <time datetime="%s" class="post-timestamp ctime">
              Posted: %s
            </time>
            <br />
            <time datetime="%s" class="post-timestamp mtime">
              Last modified: %s
            </time>
          </footer>
          <section class="post-tags">
            <h4>Tags</h4>
            <ul class="tag-list">
              %s
            </ul>
        </article>
EOT
);

function render_error($msg) {
  echo <<<EOT
    <header id="main-header" class="mini-header">
      <h2>Error</h2>
    </header>
    <div>
      $msg
    </div>
EOT;
}

function get_excerpt($body, $id) {
  if (strlen($body) > 400) {
    return substr(
      '<pre class="excerpt">' . strip_tags($body) . '</pre>',
      0,
      400
    ) . " <a href=post.php?id=$id>[continue reading]</a>";
  }
  return $body;
}

function format_time($time) {
    return date('l, j F Y', $time) . ' at ' . date('g:i:s A', $time);
}

function render_post($post, $excerpt) {
  if (!array_key_exists('Title', $post)) {
    error_log('No title');
  }

  $ctime = strtotime($post['Creation_Time']);
  $mtime = strtotime($post['Modification_Time']);

  return sprintf(
    POST_FMT,
    $post['ID'],
    $post['Title'],
    $excerpt ? get_excerpt($post['Body'], $post['ID']) : $post['Body'],
    date('c', $ctime),
    format_time($ctime),
    date('c', $mtime),
    $mtime == 0 ? 'Never' : format_time($mtime),
    '<li>(Not implemented yet)</li>'
  );
}

function get_post($id) {
  try {
    # Get DB handle
    $db = get_dbh();
    if (is_null($db)) {
      throw new Exception('Failed to get DB handle');
    }
    # Prepare the query
    $stmt = $db->prepare('SELECT * FROM Posts WHERE ID = ?');
    if (!$stmt) {
      throw new Exception('Error preparing statement');
    }
    if (!$stmt->bind_param('i', $id)) {
      throw new Exception('Error binding ID parameter');
    }
    if (!$stmt->execute()) {
      throw new Exception('Error executing statement');
    }
    # Select the post
    $res = $stmt->get_result();
    if ($row = $res->fetch_assoc()) {
      return $row;
    }
  }
  catch (Exception $e) {
    log_db_error($db, $e);
  }
  finally {
    safe_close($db);
    safe_close($stmt);
  }
  return;
}

function get_post_range($offset, $number) {
  try {
    # Get DB handle
    $db = get_dbh();
    if (is_null($db)) {
      throw new Exception('Failed to get DB handle');
    }
    # Prepare statement
    $posts = array();
    $stmt = $db->prepare(
      'SELECT * FROM Posts ORDER BY Creation_Time DESC LIMIT ? OFFSET ?'
    );
    if (is_null($stmt)) {
      throw new Exception('Error preparing statement');
    }
    if (!$stmt->bind_param('ii', $number, $offset)) {
      throw new Exception('Failed to bind parameters to fetch posts');
    }
    # Fetch rows
    if (!$stmt->execute()) {
      throw new Exception('Statement execution failed');
    }
    # Add rows to table
    $res = $stmt->get_result();
    $i = 0;
    while ($row = $res->fetch_assoc()) {
      $posts[$i++] = $row;
    }
    return $posts;
  }
  catch (Exception $e) {
    log_db_error($db, $e);
  }
  finally {
    safe_close($db);
    safe_close($stmt);
  }
  return;
}

function make_archive_list() {
  try {
    $db = get_dbh();
    if (is_null($db)) {
      throw new Exception('Failed to get DB handle');
    }

    $res = $db->query(
      'SELECT ID, Title, Creation_Time FROM Posts ORDER BY Creation_Time DESC'
    );
    if (!$res) {
      throw new Exception('Error retrieving posts');
    }
    while ($row = $res->fetch_assoc()) {
      $id = $row['ID'];
      $title = $row['Title'];
      $ctime = $row['Creation_Time'];
      printf(
        "<li><a href=\"post.php?id=%s\">%s</a>"
        . " <time datetime=\"%s\">%s</time></li>\n",
        $id,
        $title,
        date('c', strtotime($ctime)),
        format_time(strtotime($ctime)),
        '</li>'
      );
    }
  }
  catch (Exception $e) {
    echo '<p>' . e.getMessage() . '</p>';
    log_db_error($db, $e);
  }
  finally {
    safe_close($db);
  }
}

function get_post_count() {
  try {
    $db = get_dbh();
    if (is_null($db)) {
      throw new Exception('Failed to get DB handle');
    }
    $res = $db->query('SELECT COUNT(*) AS Count FROM Posts');
    if (!$res) {
      throw new Exception('Error retrieving post count');
    }
    if ($row = $res->fetch_assoc()) {
      return $row['Count'];
    }
    else {
      throw new Exception('Error getting row from result');
    }
  }
  catch (Exception $e) {
    log_db_error($db, $e);
  }
  finally {
    safe_close($db);
  }
}

?>
