<?php

require_once('util.php');

function send_response($success, $explanation, $post_id) {
  $response = json_encode([
    'post_id' => $post_id,
    'success' => $success,
    'explanation' => $explanation
  ]);
  error_log('DEBUG Sending response text: ' . $response);
  echo $response;
}

call_user_func(function() {
  if (!is_logged_in() and $_COOKIE['user_id'] == 'admin') {
    # Need to be admin to post
    http_response_code(401);
    send_response(false, 'Unauthorized access', null);
    return;
  }

  try {
    $db = get_dbh();
    if (is_null($db)) {
      throw new Exception('Failed to get DB handle');
    }
    $stmt = $db->prepare('INSERT INTO Posts(Title, Body) VALUES (?, ?)');
    if (!$stmt) {
      throw new Exception('Error preparing statement');
    }
    if (!$stmt->bind_param('ss', $_POST['postTitle'], $_POST['postBody'])) {
      throw new Exception('Error binding parameters');
    }
    if (!$stmt->execute()) {
      throw new Exception('Error executing statement');
    }
    http_response_code(201); # Created
    error_log('DEBUG Created post with ID ' . $db->insert_id);
    send_response(true, 'Post created', $db->insert_id);
  }
  catch (Exception $e) {
    log_db_error($db, $e);
    http_response_code(500);
    send_response(false, 'Internal server error', null);
  }
  finally {
    safe_close($stmt);
    safe_close($db);
  }
});

?>
