<?php

require_once('util.php');

function send_response($action, $success, $explanation) {
  http_response_code(200);
  $response = json_encode([
    'action' => $action,
    'success' => $success,
    'explanation' => $explanation
  ]);
  error_log('DEBUG Sending response text: ' . $response);
  echo $response;
}

DEFINE('SECONDS_IN_DAY', 24 * 60 * 60);

call_user_func(function() {

  parse_str($_SERVER['QUERY_STRING'], $query);

  if (!array_key_exists('action', $query)) {
    error_log('login.php called without action');
    http_response_code(500);
    exit();
  }

  // error_log(print_r($_POST, true));
  if ($query['action'] == 'login') {
    try {
      $db = get_dbh();
      if (is_null($db)) {
        throw new Exception('Failed to get DB handle');
      }
      $stmt = $db->prepare(<<<EOT
        SELECT PWHash, PWHash = SHA2(CONCAT(?, SALT), 256) AS CorrectPW
        FROM Users
        WHERE Name = ?
EOT
      );
      if (!$stmt) {
        throw new Exception('Error preparing statement');
      }
      $stmt->bind_param('ss', $_POST['password'], $_POST['username']);
      $stmt->execute();
      $res = $stmt->get_result();
      if ($row = $res->fetch_assoc()) {
        // error_log(print_r($row, true));
        if ($row['CorrectPW']) {
          $session_id = uniqid();
          $stmt2 = $db->prepare(
            'REPLACE INTO Sessions(ID, UserID) VALUES(?, ?)'
          );
          if (!$stmt2) {
            throw new Exception('Error preparing statement');
          }
          if (!$stmt2->bind_param('ss', $session_id, $_POST['username'])) {
            throw new Exception('Error binding parameters');
          }
          if (!$stmt2->execute()) {
            throw new Exception(
              'Error creating new session for user "'
              . $_POST['username']
              . '"'
            );
          }
          setcookie('user_id', $_POST['username'], time() + 7 * SECONDS_IN_DAY);
          setcookie('session_id', $session_id, time() + 7 * SECONDS_IN_DAY);
          error_log('DEBUG Created session');
          send_response('login', true, 'Login successful');
        }
        else {
          send_response('login', false, 'Incorrect password');
        }
      }
      else {
        send_response('login', false, 'Unknown user');
      }
    }
    catch (Exception $e) {
      log_db_error($db, $e);
      http_response_code(500);
      send_response('login', false, 'Internal server error');
      return;
    }
    finally {
      safe_close($stmt);
      safe_close($db);
    }
  }
  elseif ($query['action'] == 'logout') {
    if (!is_logged_in()) {
      send_response('logout', false, 'User is not logged in');
      return;
    }
    try {
      $db = get_dbh();
      if (is_null($db)) {
        throw new Exception('Failed to get DB handle');
      }
      $stmt = $db->prepare('DELETE FROM Sessions WHERE ID = ? AND UserID = ?');
      if (!$stmt) {
        throw new Exception('Error preparing statement');
      }
      if (
        !$stmt->bind_param('ss',
        $_COOKIE['session_id'],
        $_COOKIE['user_id'])
      ) {
        throw new Exception('Error binding parameters');
      }
      if (!$stmt->execute()) {
        throw new Exception('Error executing statement');
      }
      setcookie('user_id', "", time() - 3600);
      setcookie('session_id', "", time() - 3600);
      send_response('logout', true, 'Logged out successfully');
    }
    catch (Exception $e) {
      log_db_error($db, $e);
      http_response_code(500);
      send_response('logout', false, 'Internal server error');
    }
    finally {
      safe_close($stmt);
      safe_close($db);
    }
  }
  else {
    http_response_code(400);
    send_response($query['action'], false, 'Unknown action');
    error_log('login.php called with unknown query ' . $_SERVER['QUERY_STRING']);
  }
});

?>
