<?php

function get_dbh() {
  $db = new mysqli('localhost', 'root', '', 'website');
  if ($db->connect_errno) {
    error_log(
      sprintf(
        'Failed to connect to MySQL: %s (%d)',
        $db->connect_error,
        $db->connect_errno
      )
    );
    return null;
  }
  $db->select_db('website');
  return $db;
}

function log_db_error($db, $exception) {
  if (!is_null($db)) {
    error_log(
      sprintf(
        '%s: %s (%d)',
        $exception->getMessage(),
        $db->error,
        $db->errno
      )
    );
  }
  else {
    error_log($exception->getMessage());
  }
}

function safe_close($obj) {
  if ($obj) {
    if (method_exists($obj, 'close')) {
      $obj->close();
    }
    else {
      error_log(
        'safe_close() called on erroneous object: ' . print_r($obj, true)
      );
    }
  }
}

function is_logged_in() {
  if (isset($_COOKIE['user_id']) and isset($_COOKIE['session_id'])) {
    try {
      $db = get_dbh();
      if (is_null($db)) {
        throw new Exception('Failed to get DB handle');
      }
      $stmt = $db->prepare(
        'SELECT ID = ? AND UserID = ? AS LoggedIn FROM Sessions'
      );
      if (!$stmt) {
        throw new Exception('Error preparing statement');
      }
      if (!$stmt->bind_param('ss', $_COOKIE['session_id'], $_COOKIE['user_id'])) {
        throw new Exception('Error binding parameter');
      }
      if (!$stmt->execute()) {
        throw new Exception('Error executing statement');
      }
      $res = $stmt->get_result();
      if ($row = $res->fetch_assoc()) {
        return $row['LoggedIn'];
      }
      else {
        throw new Exception('Error executing query');
      }
    }
    catch (Exception $e) {
      log_db_error($db, $e);
    }
    finally {
      safe_close($stmt);
      safe_close($db);
    }
  }
  return false;
}

?>
