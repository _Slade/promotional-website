(function() {
  var post = document.getElementById('postForm');
  if (typeof(post) !== 'undefined') {
    post.postSubmit.onclick = function(e) {
      e.preventDefault();
      console.log('Submitting');
      var xhr = get_xhr();
      if (!xhr) {
        alert('Error: could not initialize request.');
        return false;
      }
      xhr.open('POST', 'new_post.php');
      xhr.onreadystatechange = function(resp) {
        if (xhr.readyState === XMLHttpRequest.DONE) {
          if ([200, 201, 202].indexOf(xhr.status) >= 0) {
            if (window.confirm('Post successful! Go to post?')) {
              location.replace('post.php?id=' + xhr.response.post_id);
            }
            else {
              location.reload();
            }
          }
          else if (xhr.status === 401) {
            alert('You are not authorized to do this.');
          }
          else if (xhr.status === 500) {
            alert('Internal server error.');
          }
          else {
            alert('Unknown error: ' + xhr.status);
          }
          return false;
        }
      }
      xhr.send(new FormData(post));
    };
  }
})();
// vim: set sw=2 ts=2 sts=2:
