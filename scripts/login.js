(function() {

  var login = document.getElementById('loginForm');
  if (typeof(login.loginSubmit) !== 'undefined') {
    setupLogin(login);
  }
  else {
    setupLogout(login);
  }

  function setupLogin(login) {
    login.loginSubmit.onclick = function() {
      if (login.username.value === "" || login.password.value === "") {
        alert('Pease fill in the required fields');
      }
      else {
        var xhr = get_xhr();
        if (!xhr) {
          alert('Error logging in: could not initialize request');
          return false;
        }
        xhr.open('POST', 'login.php?action=login');
        xhr.onreadystatechange = function(resp) {
          if (xhr.readyState === XMLHttpRequest.DONE) {
            console.log(xhr.response);
            if (xhr.response.action === 'login') {
              alert(
                xhr.response.success
                  ? 'Logged in successfully. The page will now refresh.'
                  : 'Error logging in: ' + xhr.response.explanation
              );
              location.reload();
            }
            else {
              alert('Something went wrong --- server gave a weird response.');
            }
          }
        };
        xhr.send(new FormData(login));
      }
    };

    login.username.onkeyup = function(e) {
      if (e.keyCode === 9 || e.keyCode === 13) {
        login.password.focus();
      }
    }

    login.password.onkeyup = function(e) {
      if (e.keyCode === 13) {
        login.loginSubmit.click();
      }
    };
  }

  function setupLogout(login) {
    login.logoutSubmit.onclick = function() {
      var xhr = get_xhr();
      if (!xhr) {
        alert('Error logging in: could not initialize request');
        return false;
      }
      xhr.open('POST', 'login.php?action=logout');
      xhr.onreadystatechange = function(resp) {
        if (xhr.readyState === XMLHttpRequest.DONE) {
          console.log(xhr.response);
          if (xhr.response.action === 'logout') {
            alert(
              xhr.response.success
                ? 'Logged out successfully. The page will now refresh.'
                : 'Error logging out: ' + xhr.response.explanation
            );
            location.reload();
          }
          else {
            alert('Something went wrong --- server gave a weird response.');
          }
        }
      };
      xhr.send(null);
    };
  }

})();
// vim: set sw=2 ts=2 sts=2:
