/* Miscellaneous utility functions */

function get_xhr() {
  if (window.XMLHttpRequest) {
    xhr = new XMLHttpRequest({mozSystem: true});
    if (xhr.overrideMimeType) {
      xhr.overrideMimeType('text/json');
    }
    xhr.responseType = 'json';
  }
  return xhr;
} 
